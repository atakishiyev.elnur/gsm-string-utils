/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.gsm.string.utils.bcd;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

//Dial 0-9 , * , # , A , B , C
//Hex  0-9 , B , C , A , D , E

/**
 * @author eatakishiyev
 */
public class BCDStringUtils {

    private static int encodeNumber(char c) throws IOException {
        switch (c) {
            case '0':
                return 0x00;
            case '1':
                return 0x01;
            case '2':
                return 0x02;
            case '3':
                return 0x03;
            case '4':
                return 0x04;
            case '5':
                return 0x05;
            case '6':
                return 0x06;
            case '7':
                return 0x07;
            case '8':
                return 0x08;
            case '9':
                return 0x09;
            case '*':
                return 0x0A;
            case '#':
                return 0x0B;
            case 'A':
                return 0x0C;
            case 'B':
                return 0x0D;
            case 'C':
                return 0x0E;
            default:
                throw new IOException(
                        "char should be between 0 - 9, *, #, a, b, c for Telephony Binary Coded Decimal String. Received "
                                + c);

        }
    }

    private static char decodeNumber(int i) throws IOException {
        switch (i) {
            case 0x00:
                return '0';
            case 0x01:
                return '1';
            case 0x02:
                return '2';
            case 0x03:
                return '3';
            case 0x04:
                return '4';
            case 0x05:
                return '5';
            case 0x06:
                return '6';
            case 0x07:
                return '7';
            case 0x08:
                return '8';
            case 0x09:
                return '9';
            case 0x0A:
                return '*';
            case 0x0B:
                return '#';
            case 0x0C:
                return 'A';
            case 0x0D:
                return 'B';
            case 0x0E:
                return 'C';
            case 0x0F:
                return 'F';
            default:
                throw new IOException(
                        "Integer should be between 0 - 14 for Telephony Binary Coded Decimal String. Received "
                                + i);

        }
    }

    public static int fromTBCDToByte(byte b) {
        int digit1 = b & 0x0F;
        int digit2 = (b >> 4) & 0x0f;
        return (digit1 << 4) | digit2;
    }

    public static String fromTBCDToString(InputStream ais) throws IOException {
        StringBuilder address = new StringBuilder();
        while (ais.available() > 0) {
            int b = ais.read();
            int digit1 = (b & 0x0F);
            address.append(decodeNumber(digit1));
            int digit2 = (b & 0xF0) >> 4;
            if (digit2 != 15) {
                address.append(decodeNumber(digit2));
            }
        }
        return address.toString();
    }

    public static String fromBCDToString(InputStream ais, Integer filler) throws IOException {
        int f = filler != null ? filler : 0x0f;
        StringBuilder address = new StringBuilder();
        while (ais.available() > 0) {
            int b = ais.read();
            int digit1 = (b & 0xF0) >> 4;
            address.append(decodeNumber(digit1));
            int digit2 = (b & 0x0F);
            if (digit2 != f) {
                address.append(decodeNumber(digit2));
            }
        }
        return address.toString();
    }

    public static String fromBCDToString(InputStream is) throws IOException {
        return fromBCDToString(is, null);
    }

    public static void toTBCDString(String str, OutputStream os, Integer filler) throws IOException {
        int f = filler != null ? filler : 0x0f;
        char[] digits = str.toCharArray();
        for (int i = 0; i < digits.length; i++) {
            char d = digits[i];
            int digit1 = encodeNumber(d);
            int digit2;
            if ((i + 1) == digits.length) {
                digit2 = f;
            } else {
                char a = digits[++i];
                digit2 = encodeNumber(a);
            }
            int digit = (digit2 << 4) | digit1;
            os.write(digit);
        }
    }

    public static void toTBCDString(String str, OutputStream os) throws IOException {
        toTBCDString(str, os, null);
    }

    public static void toTBCDString(int digit, OutputStream os) throws IOException {
        int digit1 = digit & 0x0F;
        int digit2 = (digit >> 4) & 0x0F;
        int t = (digit1 << 4) | digit2;
        os.write(t);
    }
}
