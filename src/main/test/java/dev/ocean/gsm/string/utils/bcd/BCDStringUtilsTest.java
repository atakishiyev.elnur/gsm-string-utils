package dev.ocean.gsm.string.utils.bcd;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class BCDStringUtilsTest {

    ByteArrayOutputStream baos ;

    @BeforeEach
    public void beforeEachTest(){
        baos = new ByteArrayOutputStream();
    }

    @Test
    public void fromTBCDToByte() {
        int result = BCDStringUtils.fromTBCDToByte((byte) 0x94);
        assertEquals(73, result);
    }

    @Test
    public void fromTBCDToString() throws IOException {
        AsnInputStream ais = new AsnInputStream(new byte[]{0x21, 0x43, 0x65, (byte) 0x87, 0x9});
        String result = BCDStringUtils.fromTBCDToString(ais);
        assertEquals("1234567890", result);
    }

    @Test
    public void fromBCDToString() throws IOException {
        AsnInputStream ais = new AsnInputStream(new byte[]{0x21, 0x43, 0x65, (byte) 0x87, 0x9});
        String result = BCDStringUtils.fromBCDToString(ais, null);
        assertEquals("2143658709", result);
    }

    @Test
    public void toTBCDString() throws IOException {
        BCDStringUtils.toTBCDString("1234567890", baos, null);
        byte[] output = baos.toByteArray();
        assertArrayEquals(new byte[]{0x21, 0x43, 0x65, (byte) 0x87, 0x9}, output);
    }

    @Test
    public void toTBCDStringWithFFiller() throws IOException {
        BCDStringUtils.toTBCDString("123456789", baos, 0x0F);
        byte[] output = baos.toByteArray();
        assertArrayEquals(new byte[]{0x21, 0x43, 0x65, (byte) 0x87,(byte)0xF9}, output);
    }

    @Test
    public void intToTBCDString() throws IOException{
        BCDStringUtils.toTBCDString(0x21, baos);
        byte[] output = baos.toByteArray();
        assertArrayEquals(new byte[]{0x12}, output);
    }
}
